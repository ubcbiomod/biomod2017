![Build Status](https://gitlab.com/pages/middleman/badges/master/build.svg)

---
## BIOMOD 2017

Welcome to the gitlab page for the biomod 2017 website! If you'd like to help out with the website side of things, you need to be willing to use *markdown* (no prior knowledge required) or go full *web-dev ninja* (heavy skill learning to really get into it).

_@adammesa_ will help install it on your computer - just make sure you let him know at a meeting, and  which of the two versions you want. You will also need to sign-up on *gitlab* and get @edwardwang1 to give you access to the BIOMOD repository.

<<<<<<< HEAD
**Frameworks quick-links**
Animate CSS - https://daneden.github.io/animate.css/

=======
>>>>>>> origin/master
##### Markdown Version

You will have the following installed on your machine

- Git
- Github for desktop (optional, recommended)
- Typora (or Atom editor) (optional)
  - Typora does a really good job of handling markdown formatting and the "frontmatter" that the pages of our system use, whereas Atom editor is a full-fledge website development system

##### Web-Dev Ninja

In order to successfully develop for the site, you'll need everything from the **markdown version** and also need to install **Middleman** so you can see test changes to the CSS and HTML on your own machine. (While it's possible to get away with just pushing your changes to the gitlab server and seeing how it affects the live site, you might be waiting ten or fifteen minutes to see your change each time you make it)

The following will be installed

* Ruby 2.2.x
* Ruby devkit for 2.2.x
* Ruby gems (if not already installed)
* Middleman ``gem install middleman``
  * Middleman will install over half-a-dozen other gem dependencies

---

Biomod Website Using Middleman.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: ruby:2.1

cache:
  paths:
  - vendor

test:
  script:
  - apt-get update -yqqq
  - apt-get install -y nodejs
  - bundle install --path vendor
  - bundle exec middleman build
  except:
    - master

pages:
  script:
  - apt-get update -yqqq
  - apt-get install -y nodejs
  - bundle install --path vendor
  - bundle exec middleman build
  artifacts:
    paths:
    - public
  only:
  - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. [Install][] Middleman
3. Generate the website: `bundle exec middleman build`
4. Preview your project: `bundle exec middleman`
5. Add content

Read more at Middleman's [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[Middleman]: https://middlemanapp.com/
[install]: https://middlemanapp.com/basics/install/
[documentation]: https://middlemanapp.com/basics/install/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
